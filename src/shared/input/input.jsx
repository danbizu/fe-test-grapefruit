import React from 'react';
import './input.scss';
import PropTypes from 'prop-types';

export default function Input(props) {
    const { value, name, onChange, icon, placeholder } = props;

    return (
        <div className="input" id="wrapper">
            <img src={icon} alt='input icon' />
            <input value={value}
                name={name}
                onChange={onChange}
                placeholder={placeholder} />
        </div>
    );
}

Input.propTypes = {
    value: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    icon: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
}