import React from 'react';
import './header.scss';
import menu from '../assets/Icon - Menu.png';
import logo from '../assets/Logo.png';
import close from '../assets/Icon - Close.png';
import PropTypes from 'prop-types';

export default function Header(props) {
    const { toggleNav } = props;

    return (
        <div id="header">
            {/** Left menu */}
            <div id="left-menu" onClick={toggleNav}>
                <img src={menu} alt="menu icon"/>
            </div>

            {/** Logo */}
            <div id="logo">
                <img src={logo} alt="logo" />
            </div>

            {/** Right menu */}
            <div id="right-menu">
                <img src={close} alt="close" />
            </div>
        </div>
    );
}

Header.propTypes = {
    toggleNav: PropTypes.func.isRequired
};