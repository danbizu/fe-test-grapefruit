import React, { useState } from 'react';
import Input from '../shared/input/input';
import './sign-up.scss';
import nameIcon from '../shared/assets/Icon - Name.png';
import emailIcon from '../shared/assets/Icon - Email.png';
import passwordIcon from '../shared/assets/Icon - Password.png';
import Steps from './steps/steps';

const STEPS = ['STEP 1', 'STEP 2', 'STEP 3'];

export default function SignUp() {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [step, setStep] = useState(0);

    function handleInputChange(ev) {
        switch (ev.currentTarget.name) {
            case 'name': {
                setName(ev.currentTarget.value);
                setStep(0);
                break;
            }
            case 'email': {
                setEmail(ev.currentTarget.value);
                setStep(1);
                break;
            }
            case 'password': {
                setPassword(ev.currentTarget.value);
                setStep(2);
                break;
            }
            default: break;
        }
    }

    return (
        <div id="sign-up">
            {/** Gradient */}
            <div id="layer-bl-corner" />
            <div id="layer-br-corner" />

            {/** Steps */}
            <Steps steps={STEPS}
                currentStep={step} />

            {/** Title */}
            <h1>CREATE ACCOUNT</h1>

            {/** Form */}
            <form>
                <Input value={name}
                    name='name'
                    onChange={ev => handleInputChange(ev)}
                    icon={nameIcon}
                    placeholder='Name' />

                <Input value={email}
                    name='email'
                    onChange={ev => handleInputChange(ev)}
                    icon={emailIcon}
                    placeholder="Email" />

                <Input value={password}
                    name='password'
                    onChange={ev => handleInputChange(ev)}
                    icon={passwordIcon}
                    placeholder="Password" />
            </form>

            {/** Submit */}
            <button>Continue</button>
            <span id="terms-and-conditions">Terms &amp; Conditions</span>
        </div>
    );
}