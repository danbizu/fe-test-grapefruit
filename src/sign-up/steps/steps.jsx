import React from 'react';
import PropTypes from 'prop-types';
import './steps.scss';

export default function Steps(props) {
    const { steps, currentStep } = props;

    return (
        <div id="steps">
            {
                steps.length &&
                steps.map((step, key) =>
                    <div key={key} className="wrapper">

                        {/** Step */}
                        <div className="step"
                            id={key == currentStep ? "current" : ""}>

                            {/** Dot */}
                            {
                                key == currentStep ?
                                    /** Outline if it's current step */
                                    <div id="outline">
                                        <div className="dot" />
                                    </div>
                                    :
                                    /** Simple dot */
                                    <div className="dot" />
                            }

                            <span>{step}</span>
                        </div> {/** /Step */}

                        {/** Add connector if it's not the last step */}
                        {
                            (key + 1) != steps.length &&
                            <div className="connector" />
                        }
                    </div>

                )
            }
        </div>
    );
}

Steps.propTypes = {
    steps: PropTypes.array.isRequired,
    currentStep: PropTypes.number.isRequired
}