import React from 'react';
import sourceIcon from '../../shared/assets/Icon - Source.png';
import timeIcon from '../../shared/assets/Icon - Time.png';
import './news-widget.scss';

export default function NewsWidget() {
    return (
        <div className="news-widget">
            <div id="background">
                <h1>Astronauts could land on Red Planet by 2039</h1>

                <footer>
                    <div id="details">
                        {/** Source */}
                        <div id="source">
                            <img src={sourceIcon} alt="Source icon" />

                            <span>SPACE.com</span>
                        </div>

                        {/** Time */}
                        <div id="time">
                            <img src={timeIcon} alt="Time icon" />

                            <span>20m ago</span>
                        </div>
                    </div>

                    <div id="domain">
                        SCIENCE
                    </div>
                </footer>
            </div>
        </div>
    );
}