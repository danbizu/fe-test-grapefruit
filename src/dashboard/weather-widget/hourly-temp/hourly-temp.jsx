import React from 'react';
import PropTypes from 'prop-types';
import './hourly-temp.scss';

export default function HourlyTemp(props) {
    const { temp, hour } = props;

    return (
        <div className="hourly-temp">
            <span className="temp">
                {temp}&deg;
            </span>

            <span className="hour">
                {hour}
            </span>
        </div>
    );
}

HourlyTemp.propTypes = {
    temp: PropTypes.number.isRequired,
    hour: PropTypes.string.isRequired
}