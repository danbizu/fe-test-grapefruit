import React from 'react';
import './weather-widget.scss';
import PropTypes from 'prop-types';
import weatherIcon from '../../shared/assets/Icon - Weather.png';
import HourlyTemp from './hourly-temp/hourly-temp';

export default function WeatherWidget(props) {
    const { weather, location, currentTemp, hourlyTemps } = props;

    return (
        <div className="weather-widget">
            {/** Top content */}
            <div id="top">

                {/** Top-Left side */}
                <div id="left">
                    <img src={weatherIcon} alt="Weather icon" />

                    {/** Sky description */}
                    <span id="sky-description">
                        {weather}
                    </span>

                    {/** Location */}
                    <span id="location">
                        {location}
                    </span>
                </div>

                {/** Top-Right side */}
                <div id="right">
                    <div id="timestamps">
                        <span className="timestamp" id="current">TODAY</span>

                        <span className="timestamp">TOMORROW</span>

                        <span className="timestamp">WEEK</span>
                    </div>

                    <span id="current-temp">
                        {currentTemp}&deg;
                    </span>
                </div>
            </div> {/** /Top content */}

            {/** Bottom content */}
            <div id="temps">
                {/** Map temps */}
                {
                    hourlyTemps.length &&
                    hourlyTemps.map((hourlyTemp, key) =>
                        <HourlyTemp key={key} {...hourlyTemp} />
                    )
                }
            </div>
        </div>
    );
}

WeatherWidget.propTypes = {
    weather: PropTypes.string.isRequired,
    location: PropTypes.string.isRequired,
    currentTemp: PropTypes.number.isRequired,
    hourlyTemps: PropTypes.array.isRequired
}