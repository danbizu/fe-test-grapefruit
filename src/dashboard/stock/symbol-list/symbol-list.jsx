import React from 'react';
import './symbol-list.scss';
import PropTypes from 'prop-types';
import StockSymbol from './stock-symbol/stock-symbol';

export default function SymbolList(props) {
    const { symbols } = props;

    return (
        <div className="symbol-list">
            {
                symbols.length &&
                symbols.map((symbol, key) =>
                    <StockSymbol key={key}
                        symbol={symbol.name}
                        change={symbol.change}
                        value={symbol.value} />
                )
            }
        </div>
    );
}

SymbolList.propTypes = {
    symbols: PropTypes.array.isRequired
}