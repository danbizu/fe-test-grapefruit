import React from 'react';
import PropTypes from 'prop-types';
import './stock-symbol.scss';

export default function StockSymbol(props) {
    const { symbol, change, value } = props;

    return (
        <div className="stock-symbol">
            <div className="details">
                <span className="symbol">
                    {symbol}
                </span>

                <span className="value">
                    {value}
                </span>
            </div>

            <div className={`change${change < 0 ? ' negative' : ''}`}>
                {change}%
            </div>
        </div>
    );
}

StockSymbol.propTypes = {
    symbol: PropTypes.string.isRequired,
    change: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired
}