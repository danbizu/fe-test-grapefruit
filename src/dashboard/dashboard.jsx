import React, { useEffect, useState } from 'react';
import './dashboard.scss';
import NewsWidget from './news-widget/news-widget';
import WeatherWidget from './weather-widget/weather-widget';
import * as utils from './dashboard.utils';
import SymbolList from './stock/symbol-list/symbol-list';

export default function Dashboard() {
    const [error, setError] = useState(null);
    const [weather, setWeather] = useState(null);
    const [forecast, setForecast] = useState(null);
    const [stock, setStock] = useState(null);
    const [symbol, setSymbol] = useState('ibm');

    /** Get the weather only once */
    useEffect(() => {
        utils.getWeather()
            .then(values => {
                setWeather(values[0]);
                setForecast(values[1]);
            })
            .catch(err => {
                setError(err);
            });
    }, [])

    /** Get stock prices whenever the symbol changes */
    useEffect(() => {
        utils.getStockPrices()
            .then(function (values) {
                setStock(values);
            })
            .catch(err => {
                setError(err);
            });
    }, []);

    /** If an error occurs don't load the page. */
    if (error) {
        return <p>There was a problem attempting to load the page. Please try again!</p>
    }

    console.log('stock ', stock);
    return (
        <div>
            <div id="widgets">
                {/** News widget */}
                <NewsWidget />

                {/** Weather widget */}
                {
                    (weather && forecast) &&
                    <WeatherWidget {...utils.getWeatherProps(weather, forecast)} />
                }
            </div>

            <div id="stock">
                <SymbolList symbols={utils.SS} />
                <div id="graph">Graph</div>
            </div>
        </div>
    );
}
