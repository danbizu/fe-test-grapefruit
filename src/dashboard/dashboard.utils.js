export const SYMBOLS = ['NDAQ', 'AAPL', 'DOW', 'GOOG'];
export const SS = [{
    name: SYMBOLS[0],
    change: -0.85,
    value: 5055.55
}, {
    name: SYMBOLS[1],
    change: 0.51,
    value: 126.56
}, {
    name: SYMBOLS[2],
    change: -0.34,
    value: 19926.02
}, {
    name: SYMBOLS[3],
    change: 0.70,
    value: 534.53
}]

/** Filter and return the required props for WeatherWidget */
export function getWeatherProps(weather, forecast) {
    // Validate that we have data.
    let condition = !!(weather && forecast && !weather.error && !forecast.error);

    if (condition) {
        let props = {
            weather: '',
            location: '',
            currentTemp: 0,
            hourlyTemps: [],
        };

        let weatherResponse = weather.response[0];
        let forecasts = forecast.response[0].periods;

        // Update values
        props.weather = weatherResponse.periods[0].weather; // Weather, ex: Cloudy
        props.location = capitalizeString(weatherResponse.place.name); // Location, ex: New York
        props.currentTemp = Math.ceil(weatherResponse.periods[0].tempF); // Temp in Fahrenheit

        // Filter only the temp and hour properties.
        props.hourlyTemps = forecasts.map(f => {
            return {
                temp: f.tempF,
                hour: formatHour(f.validTime)
            }
        })
        return props;
    }
}

/** Return hour in 1-12h AM/PM format */
export function formatHour(date) {
    let hour = new Date(date).getHours();

    // When midnight return 12 AM instead
    if (hour == 0) {
        return `12 AM`;
    }

    if (hour <= 12) {
        // For noon return 12 PM
        return hour == 12 ? `12 PM` : `${hour} AM`;
    } else {
        return `${hour - 12} PM`;
    }
}

/** Return a string with each word capitalized */
export function capitalizeString(string) {
    let sep = string.split(" ");
    // Capitalize each substring
    sep = sep.map(substr => substr.charAt(0).toUpperCase() + substr.slice(1));

    // Join strings together
    let cap = sep.join(" ");
    return cap;
}

/** Call weather and forecast api */
export async function getWeather() {
    let weatherResponse = await callApi('http://localhost:8080/api/weather/newyork/ny');
    let forecastResponse = await callApi('http://localhost:8080/api/forecast/newyork/ny');

    return Promise.all([weatherResponse, forecastResponse]);
}

/** Call stock prices api */
export async function getStockPrices() {
    let NDAQResponse = await callApi(`http://localhost:8080/api/stock/${SYMBOLS[0]}`),
        AAPLResponse = await callApi(`http://localhost:8080/api/stock/${SYMBOLS[1]}`),
        DOWResponse = await callApi(`http://localhost:8080/api/stock/${SYMBOLS[2]}`),
        GOOGResponse = await callApi(`http://localhost:8080/api/stock/${SYMBOLS[3]}`);

    return Promise.all([NDAQResponse, AAPLResponse, DOWResponse, GOOGResponse]);
}

/** Call api at specified url */
export async function callApi(url) {
    let response = await fetch(url)
        .then(res => res.json())
        .catch(err => err);

    return response;
}