import './App.css';
import {
	BrowserRouter as Router,
	Link,
	Switch,
	Route,
} from 'react-router-dom';
import SignUp from './sign-up/sign-up';
import Dashboard from './dashboard/dashboard';
import Header from './shared/header/header';
import { useState } from 'react';

function App() {
	const [extendNav, setExtendNav] = useState(false);

	return (
		<Router>
			<div className="App">
				<Header toggleNav={() => setExtendNav(!extendNav)}/>

				{
					extendNav &&
					<nav>
						<ul>
							<li>
								<Link to="/sign-up">Sign Up</Link>
							</li>

							<li>
								<Link to="/">Dashboard</Link>
							</li>
						</ul>
					</nav>
				}

				<Switch>
					<Route path="/sign-up">
						<SignUp />
					</Route>

					<Route path="/">
						<Dashboard />
					</Route>
				</Switch>
			</div>
		</Router>
	);
}

export default App;
