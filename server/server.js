import express from 'express';
import Dashboard from './dashboard/dashboard';
import cors from 'cors'

const server = express();
const PORT = 8080;

// Setup cors
server.use(cors());

// Init modules
Dashboard(server);

server.listen(PORT);