import https from 'https';
import dotenv from 'dotenv';
import { resolve } from 'path';

// Set the path to the .env file.
dotenv.config({
    path: resolve(__dirname, '../.env')
});

export function getWeatherResponse(city, state) {
    const REQ_URL = `https://api.aerisapi.com/conditions/${city},${state}?client_id=${process.env.WEATHER_CLIENT_ID}&client_secret=${process.env.WEATHER_CLIENT_SECRET}`;
    return new Promise(function (resolve, reject) {
        const request = https.request(REQ_URL, response => {

            // Create empty buffer
            let buff = Buffer.from('');

            // When receiving data
            response.on('data', data => {
                // Add to buffer
                buff = Buffer.concat([buff, data]);
            })

            // When completed
            response.on('end', function () {
                // Send response
                resolve(JSON.parse(buff.toString()));
            })
        })

        request.on('error', error => {
            reject(error);
        });

        // Stop stream
        request.end();
    })
}

export function getForecastResponse(city, state) {
    const REQ_URL = `https://api.aerisapi.com/forecasts/${city},${state}?filter=3hr&limit=8&client_id=${process.env.WEATHER_CLIENT_ID}&client_secret=${process.env.WEATHER_CLIENT_SECRET}`;
    return new Promise(function (resolve, reject) {
        const request = https.request(REQ_URL, response => {

            // Create empty buffer
            let buff = Buffer.from('');

            // When receiving data
            response.on('data', data => {
                // Add to buffer
                buff = Buffer.concat([buff, data]);
            })

            // When completed
            response.on('end', function () {
                // Send response
                resolve(JSON.parse(buff.toString()));
            })
        })

        request.on('error', error => {
            reject(error);
        });

        // Stop stream
        request.end();
    })
}

export function getStockResponse(symbol) {
    const REQ_URL = `https://www.alphavantage.co/query?function=TIME_SERIES_MONTHLY_ADJUSTED&symbol=${symbol}&apikey=${process.env.ALPHAVANTAGE_API_KEY}`;

    return new Promise((resolve, reject) => {
        const request = https.request(REQ_URL, res => {

            let completeStream = Buffer.from('');
            res.on('data', data => {
                completeStream = Buffer.concat([completeStream, data]);
            })

            res.on('end', () => {
                resolve(JSON.parse(completeStream.toString()))
            });
        })

        request.on('error', error => {
            reject(error)
        })

        request.end();
    })
}