import {
    Router
} from 'express';
import dotenv from 'dotenv';
import {
    getForecastResponse,
    getStockResponse,
    getWeatherResponse
} from './dashboard.service';

dotenv.config();

const DashboardWebApi = Router();

DashboardWebApi.get('/weather/:city/:state', async function (req, res) {
    const {
        city,
        state
    } = req.params;

    let weatherResponse = await getWeatherResponse(city, state);

    res.send(weatherResponse);
})

DashboardWebApi.get('/forecast/:city/:state', async function (req, res) {
    const {
        city,
        state
    } = req.params;

    let forecastResponse = await getForecastResponse(city, state);

    res.send(forecastResponse);
})

DashboardWebApi.get('/stock/:symbol', async function (req, res) {
    const {
        symbol
    } = req.params;

    try {
        let stock = await getStockResponse(symbol);

        // Keys seem to be ordered from current date to older dates
        let last12Keys = Object.keys(stock['Monthly Adjusted Time Series']).slice(0, 12);

        // Get only last 12 months
        let lastYear = [];
        last12Keys.forEach(date => {
            lastYear.push({
                ...stock['Monthly Adjusted Time Series'][date],
                date: date // Add the date as well
            });
        })

        res.send(lastYear);
    } catch (err) {
        res.status(err.statusCode).send(err.message);
    }
})

export default function Dashboard(app) {
    app.use('/api', DashboardWebApi)
}